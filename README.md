# Zadanie

## Vytovrit aplikaciu kde sa da objednat produkt:
- Entity:
    - Produkt: nazov, cena
    - Objednavka: produkt_id (obj moze mat viacero produktov), cena, meno zakaznika, email zakanzika
- Admin rozhranie:
    - pristup pre admina kde moze: listovat/pridavat/upravovat/mazat produkty
    - listovat objednavky
- Verejne rozhranie:
    - vyber produktov: ponuknut vsetky produkty z DB a zakaznik si moze vybrat minimalne 1 alebo viacero. nebude sa dat vybrat 2x rovnaky produkt
    - zadanie udajov: meno, email (pouzit symfony form)
    - po vytvoreni obj ulozit obj do DB + zaslat na email zakaznikovi (cez event listener)
- API:
    - stateless API call, authentifikacia cez header
    - 1 funkcia/call: vypis vsetkych objednavok (pre kazdu obj vypisat aj nazov a cenu produktu) v json

## Co pouzit v projekte:
- service: na vytvaranie objednavok, po vyvoreni vypusti event
- event: event listener ktory pocuva na vytvorenie obj a posle email zakanzikovi
- authentifikacia: inmemory hesla (https://symfony.com/doc/curr/components/security/authentication.html)

## Ako odovzdavat:
- pull request do tohto repository

## Pomocky:
symfony doc: https://symfony.com/doc/current/
